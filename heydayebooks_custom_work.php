<?php
/*
Plugin Name: Heydey Ebooks Custom Features
Plugin URI: http://codup.io/
Description: This plugin adds the custom fields to products it also covers the additional functions to fulfill gaps in the required functionality.
Version: 1.1.1.0
Author: Codup.io
Author URI: http://codup.io/
*/

class heyDayeBooks{
    
    public function __construct() {
        add_action("woocommerce_account_content", array($this,"display_gravityform_account_details"), 100);
        
        // create custom plugin settings menu
        add_action('admin_menu', array($this,'show_plugin_menu'));
        
        // attaching header scripts
        add_action('wp_enqueue_scripts', array($this, 'heydayebooks_assets'));
        
        // add custom meta fields in add/edit products
        add_action("wcv_before_inventory_tab", array($this, "wcv_custom_product_meta_fields"));
        
        // save custom meta product fields
        add_action ('wcv_save_product', array($this, 'wcv_save_product_meta_fields'));
    }
    
    /**
     * form builder method show product meta fields on add/edit product on wc vendor frontend
     * @param int $object_id -- current product id
     */
    public function wcv_custom_product_meta_fields($object_id){
        WCVendors_Pro_Form_Helper::input(
            array( 
                'post_id' 		=> $object_id, 
                'id'	 		=> '_wcv_custom_product_example_text_input', 
                'label' 		=> __( 'Product Meta Text Input', 'wcvendors-pro' ),
                'placeholder' 	=> __( 'Product Meta Text Input', 'wcvendors-pro' ), 
                'desc_tip' 		=> 'true', 
                'description' 	=> __( 'This displays under the field. ', 'wcvendors-pro' ), 
                'custom_attributes' => array( 
                        'data-rules' => 'required', 
                        'data-error' => __( 'This is a required field.', 'wcvendors-pro' )
                    )
            )
        );
    
        WCVendors_Pro_Form_Helper::select2(array(
                'post_id' => $object_id,
                'id' => 'service[]',
                'label' => __( 'Service Type', 'wc_simple_hosts' ),
                'wrapper_start' => '<div class=”all-100″>',
                'wrapper_end' => '</div>',
                'taxonomy'	=> 'pa_service',
                'taxonomy_args'	=> array( 'hide_empty'=>false ),
                'custom_attributes' => array( 'multiple' => 'multiple', ),
                'class' => 'select2',
        ));
    
        function wcv_file_uploader( ){
            $value = get_user_meta( get_current_user_id(), 'YOURMETAKEYHERE', true ); 
            WCVendors_Pro_Form_Helper::file_uploader( array(  
                    'post_id' => $object_id,
                    'header_text'		=> __('File uploader', 'wcvendors-pro' ), 
                    'add_text' 			=> __('Add file', 'wcvendors-pro' ), 
                    'remove_text'		=> __('Remove file', 'wcvendors-pro' ), 
                    'image_meta_key' 	=> 'YOURMETAKEYHERE', 
                    'save_button'		=> __('Add file', 'wcvendors-pro' ), 
                    'window_title'		=> __('Select an Image', 'wcvendors-pro' ), 
                    )
            );
        }

        // call this function in your template 
        wcv_file_uploader();
    }
    
    /**
     * save/update product custom meta
     * @param int $product_id
     */
    public function wcv_save_product_meta_fields($product_id){
        update_post_meta( $product_id, '_wcv_custom_product_example_text_input', $_POST['_wcv_custom_product_example_text_input'] );
    }
    
    /**
     * enqueue the style sheet in wordpress head
     */
    public function heydayebooks_assets(){
        wp_enqueue_style('style', plugin_dir_url(__FILE__) . 'assets/css/custom.css',false,'1.1','all');
    }

    /**
     * displaying gravity form via shortcode on woocommerce account detail page
     */
    public function display_gravityform_account_details(){
        // loading account detail template
        include "templates/account_details.php";
    }
    
    
    public function show_plugin_menu() {
        //create new top-level menu
        add_menu_page('Hey Day eBooks', 'Hey Day eBooks', 'administrator', __FILE__, array($this,'display_heydayebooks_settings_page') );
    }
    
    // callback function for settings page
    public function display_heydayebooks_settings_page() {
        // on submit update options
        if (isset($_POST['update_user_gravity_form_id'])){
            $gravity_form_id = $_POST['update_user_gravity_form_id'];
            update_option("update_user_gravity_form_id", $gravity_form_id);
        }
        
        // loading account detail template
        include "templates/plugin_settings.php";
    }
}

new heyDayeBooks();







function wpse125952_redirect_to_request( $redirect_to, $request, $user ){
    // instead of using $redirect_to we're redirecting back to $request
    return $request;
}
add_filter('login_redirect', 'wpse125952_redirect_to_request', 10, 3);
?>