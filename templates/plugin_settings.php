<div class="wrap">
<h2>Hey Day eBooks Settings</h2>

<form method="post">
    <table class="form-table">
        <tr valign="top">
            <th scope="row">Update user Gravity form ID</th>
            <td><input type="text" name="update_user_gravity_form_id" value="<?php echo esc_attr( get_option('update_user_gravity_form_id') ); ?>" /></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
</div>